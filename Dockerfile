FROM google/cloud-sdk:alpine
RUN apk add --update coreutils
RUN gcloud components install kubectl
RUN gcloud components install beta
RUN gcloud components update